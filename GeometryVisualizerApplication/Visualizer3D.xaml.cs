﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;


using System.Windows.Media.Media3D;
using UnitClassLibrary;
using GeometryClassLibrary;

namespace GeometryVisualizerApplication {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
    /// 
    /// <remarks>
    /// code taken from  Dario Solera and r.stropek
    /// http://www.codeproject.com/Articles/23332/WPF-D-Primer
    /// http://www.codeproject.com/Articles/14414/Generating-a-sphere-mesh-in-XAML
    /// http://kindohm.com/technical/WPF3DTutorial.htm
    /// </remarks>
    /// 
	/// </summary>
    public partial class Visualizer3D : UserControl
    {

        private ModelVisual3D mGeometry;
		private bool mDown;
		private System.Windows.Point mLastPos;
        

		public Visualizer3D() {
            InitializeComponent();

        }

        private void draw_cube(object sender, RoutedEventArgs e)
        {
            var height = new Distance(DistanceType.Inch, 5);
            var width = new Distance(DistanceType.Inch, 5);
            var length = new Distance(DistanceType.Inch, 5);
            RectangularPrism prism = new RectangularPrism(width, height, length);


            DrawRectangularPrism(prism);


        }

        private void draw_prism(object sender, RoutedEventArgs e)
        {
            var height = new Distance(DistanceType.Inch, 5);
            var width = new Distance(DistanceType.Inch, 10);
            var length = new Distance(DistanceType.Inch, 5);
            RectangularPrism prism = new RectangularPrism(width, height, length);


            DrawRectangularPrism(prism);
        }

        private void draw_point(object sender, RoutedEventArgs e)
        {
            var height = new Distance(DistanceType.Inch, 5);
            var width = new Distance(DistanceType.Inch, 5);
            var length = new Distance(DistanceType.Inch, 5);
            RectangularPrism prism = new RectangularPrism(width, height, length);
            var point = prism.LineSegments[0].BasePoint;

            AddGeometryLibraryPointToCanvas(new GeometryClassLibrary.Point(point));
        }


        public void AddGeometryLibraryPointToCanvas(GeometryClassLibrary.Point passedPoint)
        {
            var height = new Distance(DistanceType.Millimeter, 50);
            var width = new Distance(DistanceType.Millimeter, 50);
            var length = new Distance(DistanceType.Millimeter, 50);
            RectangularPrism prism = new RectangularPrism(width, height, length);

            DrawRectangularPrism(prism);

        }


        public void DrawLineSegment(GeometryClassLibrary.LineSegment passedLineSegment)
        {
            Model3DGroup line = new Model3DGroup();
            var points = GeometryClassLibraryToWPFAdapter.MakeArrayOf3DPointsFromGeometryLineSegment(passedLineSegment);
            //Console.WriteLine(points[0]);
            //Console.WriteLine(points[1]);


            line.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[0], points[1], points[0]));

            mGeometry = new ModelVisual3D();
            mGeometry.Content = line;

        }

        
        public void DrawRectangularPrism(RectangularPrism passedPrism)
        {
            Model3DGroup prism = new Model3DGroup();
            var points = GeometryClassLibraryToWPFAdapter.MakeArrayOf3DPointsFromArrayOfGeometryLineSegments(passedPrism.LineSegments);

                //front side triangles
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[3], points[2], points[6]));
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[3], points[6], points[7]));
            //right side triangles
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[2], points[1], points[5]));
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[2], points[5], points[6]));
            //back side triangles
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[1], points[0], points[4]));
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[1], points[4], points[5]));
            //left side triangles
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[0], points[3], points[7]));
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[0], points[7], points[4]));
            //top side triangles
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[7], points[6], points[5]));
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[7], points[5], points[4]));
            //bottom side triangles
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[2], points[3], points[0]));
            prism.Children.Add(WPFDrawingHelper.CreateTriangleModel(points[2], points[0], points[1]));

            mGeometry = new ModelVisual3D();
            mGeometry.Content = prism;

            mGeometry.Transform = new Transform3DGroup();
            this.viewport.Children.Add(mGeometry);

        }

      


        private void Grid_MouseWheel(object sender, MouseWheelEventArgs e)
        {
          //  PerspectiveCamera camera = (PerspectiveCamera)viewport.Camera;
            camera.Position = new Point3D(camera.Position.X, camera.Position.Y, camera.Position.Z - e.Delta / 50D);
        }

        private void Reset(object sender, RoutedEventArgs e)
        {
            camera.Position = new Point3D(0, 0, 13.6);
            mGeometry.Content = null;
            mGeometry.Transform = new Transform3DGroup();

        }


        private void Initialize(object sender, RoutedEventArgs e)
        {
            camera.Position = new Point3D(0, 0, 13.6);
            mGeometry.Transform = new Transform3DGroup();

        }


		private void Grid_MouseMove(object sender, MouseEventArgs e) {
			if(mDown) {
                System.Windows.Point pos = Mouse.GetPosition(viewport);
                System.Windows.Point actualPos = new System.Windows.Point(pos.X - viewport.ActualWidth / 2, viewport.ActualHeight / 2 - pos.Y);
				double dx = actualPos.X - mLastPos.X, dy = actualPos.Y - mLastPos.Y;

				double mouseAngle = 0;
				if(dx != 0 && dy != 0) {
					mouseAngle = Math.Asin(Math.Abs(dy) / Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2)));
					if(dx < 0 && dy > 0) mouseAngle += Math.PI / 2;
					else if(dx < 0 && dy < 0) mouseAngle += Math.PI;
					else if(dx > 0 && dy < 0) mouseAngle += Math.PI * 1.5;
				}
				else if(dx == 0 && dy != 0) mouseAngle = Math.Sign(dy) > 0 ? Math.PI / 2 : Math.PI * 1.5;
				else if(dx != 0 && dy == 0) mouseAngle = Math.Sign(dx) > 0 ? 0 : Math.PI;

				double axisAngle = mouseAngle + Math.PI / 2;

				Vector3D axis = new Vector3D(Math.Cos(axisAngle) * 4, Math.Sin(axisAngle) * 4, 0);

				double rotation = 0.01 * Math.Sqrt(Math.Pow(dx, 2) + Math.Pow(dy, 2));

				Transform3DGroup group = mGeometry.Transform as Transform3DGroup;
				QuaternionRotation3D r = new QuaternionRotation3D(new Quaternion(axis, rotation * 180 / Math.PI));
				group.Children.Add(new RotateTransform3D(r));

				mLastPos = actualPos;
			}
		}

		private void Grid_MouseDown(object sender, MouseButtonEventArgs e) {
			if(e.LeftButton != MouseButtonState.Pressed) return;
			mDown = true;
            System.Windows.Point pos = Mouse.GetPosition(viewport);
            mLastPos = new System.Windows.Point(pos.X - viewport.ActualWidth / 2, viewport.ActualHeight / 2 - pos.Y);
		}

		private void Grid_MouseUp(object sender, MouseButtonEventArgs e) {
			mDown = false;
		}

        private void Grid_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                e.Handled = true;
                camera.Position = new Point3D(camera.Position.X + 5, camera.Position.Y, camera.Position.Z);
               // Console.WriteLine(camera.Position);
               // System.Diagnostics.Debug.Print("left");
            }
            if (e.Key == Key.Right)
            {
                e.Handled = true;
                camera.Position = new Point3D(camera.Position.X - 5, camera.Position.Y, camera.Position.Z);
                //Console.WriteLine(camera.Position);
                //System.Diagnostics.Debug.Print("Right");
            }
            if (e.Key == Key.Up)
            {
                e.Handled = true;
                camera.Position = new Point3D(camera.Position.X, camera.Position.Y - 5, camera.Position.Z);
                //Console.WriteLine(camera.Position);
               // System.Diagnostics.Debug.Print("Up");
            }
            if (e.Key == Key.Down)
            {
                e.Handled = true;
                camera.Position = new Point3D(camera.Position.X, camera.Position.Y + 5, camera.Position.Z);
               // Console.WriteLine(camera.Position);
                //System.Diagnostics.Debug.Print("Down");
            }
        }
    
    }



}
