﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;


using System.Windows.Media.Media3D;
using UnitClassLibrary;
using GeometryClassLibrary;

namespace GeometryVisualizerApplication
{
    class GeometryClassLibraryToWPFAdapter
    {

        public static Point3D[] MakeArrayOf3DPointsFromArrayOfGeometryLineSegments(List<GeometryClassLibrary.LineSegment> passedLineSegments)
        {
            int numberOfPoints = passedLineSegments.Count() * 2;
            Point3D[] points = new Point3D[numberOfPoints];

            for (int i = 0; i < passedLineSegments.Count(); i++)
            {
                points[i] = MakePoint3DFromGeometryPoint(passedLineSegments[i].BasePoint);
                points[i + 1] = MakePoint3DFromGeometryPoint(passedLineSegments[i].EndPoint);
            }

            return points;
        }

        public static Point3D[] MakeArrayOf3DPointsFromGeometryLineSegment(GeometryClassLibrary.LineSegment passedLineSegment)
        {
            var point1 = MakePoint3DFromGeometryPoint(passedLineSegment.BasePoint);
            var point2 = MakePoint3DFromGeometryPoint(passedLineSegment.EndPoint);

            Point3D[] points = new Point3D[2];
            points[0] = point1;
            points[1] = point2;

            return points;
        }

        public static Point3D MakePoint3DFromGeometryPoint(GeometryClassLibrary.Point passedPoint)
        {
            var x = passedPoint.X.Inches;
            var y = passedPoint.Y.Inches;
            var z = passedPoint.Z.Inches;

            Point3D point = new Point3D(x, y, z);

            return point;

        }






    }
}
