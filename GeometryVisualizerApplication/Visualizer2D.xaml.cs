﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GeometryVisualizerApplication
{
    /// <summary>
    /// Interaction logic for GeometryVisualizer.xaml
    /// </summary>
    public partial class Visualizer2D : UserControl
    {
        public void AddGeometryLibraryPointToCanvas()
        {

            //Console.Out.WriteLine(passedPoint.X.Inches + ", " + passedPoint.Y.Inches);

            // Create a red Rectangle.
            //Rectangle myRectangle = new Rectangle();

            // Create a SolidColorBrush with a red color to fill the  
            // Ellipse with.
            //SolidColorBrush mySolidColorBrush = new SolidColorBrush();

            //mySolidColorBrush.Color = Colors.Yellow;
            //myRectangle.Fill = mySolidColorBrush;
            //myRectangle.StrokeThickness = 2;
            //myRectangle.Stroke = Brushes.Black;

            // Set the width and height of the Rectangle.
            //myRectangle.Width = 150;
            //myRectangle.Height = 150;

            //myRectangle.Margin = new Thickness(75); //new Thickness(passedPoint.X.Millimeters, passedPoint.Y.Millimeters, passedPoint.X.Millimeters, passedPoint.Y.Millimeters);

            //GeometryCanvas.Children.Add(myRectangle);

        }

        public Visualizer2D()
        {
            InitializeComponent();
            AddGeometryLibraryPointToCanvas();

        }
    }
}
