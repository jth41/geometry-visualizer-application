﻿//using PrefabricatedComponentTypeLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeometryVisualizerApplication
{
    /// <summary>
    /// Interaction logic for DoubleVisualizerPage.xaml
    /// </summary>
    public partial class DoubleVisualizerPage : Page
    {
        public StackPanel MasterStackPanel = new StackPanel();

        public Visualizer3D Visualizer3D = new Visualizer3D();
        public Visualizer2D Visualizer2D = new Visualizer2D();
        //public RoofTruss CurrentTruss;

        public DoubleVisualizerPage()
        {
            InitializeComponent();

            MasterStackPanel.Orientation = Orientation.Horizontal;
            MasterStackPanel.Children.Add(Visualizer3D);
            MasterStackPanel.Children.Add(Visualizer2D);

            this.Content = MasterStackPanel;
            
        }
    }
}
